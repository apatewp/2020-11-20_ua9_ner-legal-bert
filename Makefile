.PHONY: pip_compile pip_install pip_update jupyter clean

ACTIVATE_VENV = source .venv/bin/activate

clean:

mk_venv:
	python3 -m venv .venv && $(ACTIVATE_VENV) && pip3 install --upgrade pip && pip3 install pip-tools

pip_install:
	$(ACTIVATE_VENV) && pip install -r requirements.txt

install: mk_venv pip_install

pip_compile:
	$(ACTIVATE_VENV) && pip-compile --verbose requirements.in > requirements.txt

pip_update: pip_compile pip_install


jupyter:
	$(ACTIVATE_VENV) && jupyter lab .


TAG = 2020-11-20_ua9_ner-legal-bert:latest
PORT = 8890
RUNTIME=--gpus all # or --runtime=nvidia or leave empty for standard runtime

build:
	docker build -t $(TAG) .

run:
	docker run --mount "type=bind,src=`pwd`,dst=/notebooks" $(RUNTIME) --rm -it  -p $(PORT):8888  $(TAG)
